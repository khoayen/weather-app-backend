// import "babel-dotenv" // require("dotenv").config()
import "@babel/polyfill"
process.env.SUPPRESS_NO_CONFIG_WARNING = "y";

require('custom-env').env('staging')

import { DatabaseService } from "./api/services/database.service";

const express = require("express");
const bodyParser = require("body-parser");
const YAML = require("yamljs");
const app = express();
const swaggerUi = require("swagger-ui-express");
// const swaggerDocument = YAML.load('./api/swagger/swagger.yaml');
const swaggerDocument = require("./api/swagger/swagger.json");
// const SwaggerExpress = require('swagger-express-mw');

import { placeRouter } from "./api/routers/place.router";
import {
  notFoundError,
  errorHandler
} from "./api/commons/middlewares/errorHandler";

const databaseService = new DatabaseService();

const port = process.env.PORT || 8901;

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use("/api/v1", placeRouter);

app.use(notFoundError, errorHandler);

app.listen(port, err => {
  console.log(`Server listening on port ${port}` || err);
});

export { app };

// app.use('/api-docs', function (req, res, next) {
//     req.swaggerDoc = swaggerDocument;
//     next();
// }, swaggerUi.serve, swaggerUi.setup());

// var config = {
//     appRoot: __dirname
// };

// SwaggerExpress.create(config, function (err, swaggerExpress) {
//     if (err) {
//         throw err;
//     }

//     // install middleware
//     swaggerExpress.register(app);

//     console.log("Server is started by port " + port);
//     app.listen(port);
// });
