const app = require("./index").app;

process.env.NODE_ENV = "staging";

let chai = require("chai");
let chaiHttp = require("chai-http");

let should = chai.should();
let expect = chai.expect;

const { Place } = require("./api/models/place.model");

chai.use(chaiHttp);

before(done => {
  Place.deleteMany({}, err => {
    done();
  });
});

describe("Places", () => {
  describe("/GET places", () => {
    it("It should return empty", done => {
      chai
        .request(app)
        .get("/api/v1/place")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          res.body.result.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/POST place", () => {
    it("It should return error, lack of required fields", done => {
      let place = {
        address: "Alston House, 3 South Rd, Lancaster LA1 4XQ, UK",
        lat: "54.0445591tyuyu"
      };
      chai
        .request(app)
        .post("/api/v1/place")
        .send(place)
        .end((err, res) => {
          res.should.have.status(412);
          expect(res.body.error).to.include(
            "'long' is required",
            "'long' is required"
          );
          done();
        });
    });
  });

  describe("/POST place", () => {
    it("It should add new place", done => {
      let place = {
        address: "Alston House, 3 South Rd, Lancaster LA1 4XQ, UK",
        lat: 54.0445591,
        long: -2.797777999999994,
        name: "FGH Security",
        rating: 3.8,
        phoneNumber: "01524 847554"
      };
      chai
        .request(app)
        .post("/api/v1/place")
        .send(place)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.result.should.be.a("object");
          done();
        });
    });
  });

  describe("/POST place", () => {
    it("It should return error, duplicate entity", done => {
      let place = {
        address: "Alston House, 3 South Rd, Lancaster LA1 4XQ, UK",
        lat: 54.0445591,
        long: -2.797777999999994,
        name: "FGH Security",
        rating: 3.8,
        phoneNumber: "01524 847554"
      };
      chai
        .request(app)
        .post("/api/v1/place")
        .send(place)
        .end((err, res) => {
          res.should.have.status(412);
          res.body.error.should.equal("Place has already been saved");
          done();
        });
    });
  });

  describe("/GET places", () => {
    it("It should return 1 place", done => {
      chai
        .request(app)
        .get("/api/v1/place")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          res.body.result.length.should.be.eql(1);
          done();
        });
    });
  });

  afterEach(() => {});
});

describe("Place", () => {
  describe("/GET place", () => {
    it("Should return not found error (not found place)", done => {
      chai
        .request(app)
        .get("/api/v1/place/5df73a22f468a30b00b3fa4f")
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/GET place", () => {
    it("Should return 500 error (wrong _id format)", done => {
      chai
        .request(app)
        .get("/api/v1/place/5df73a22f468a30b00b3fa4f4")
        .end((err, res) => {
          //   console.log(res.body);
          res.should.have.status(500);
          done();
        });
    });
  });

  describe("/GET place", () => {
    it("Should return place", done => {
      let id;
      let address;
      chai
        .request(app)
        .get("/api/v1/place")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          res.body.result.length.should.be.eql(1);
          id = res.body.result[0]._id;
          address = res.body.result[0].address;

          chai
            .request(app)
            .get(`/api/v1/place/${id}`)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.an("object");
              res.body.result.address.should.equal(address);
              done();
            });
        });
    });
  });

  describe("/DELETE place", () => {
    it("Should return empty list", done => {
      let id;
      let address;
      chai
        .request(app)
        .get("/api/v1/place")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.result.should.be.a("array");
          res.body.result.length.should.be.eql(1);
          id = res.body.result[0]._id;
          address = res.body.result[0].address;

          chai
            .request(app)
            .delete(`/api/v1/place/${id}`)
            .end((err, res) => {
              res.should.have.status(204);
              console.log(err, res.body);
              chai
                .request(app)
                .get("/api/v1/place")
                .end((err, res) => {
                  console.log("RES", res.body);
                  res.should.have.status(200);
                  res.body.result.should.be.a("array");
                  res.body.result.length.should.be.eql(0);
                  done();
                });
            });
        });
    });
  });
});
