import { connect } from "mongoose";

const DB_URI = process.env.DB_URI || "mongodb+srv://khoayen:khoamalab_B1@cluster0-yvppm.mongodb.net/test__KHOA";

// const DB_URI = "mongodb://localhost:27017/weather-app"
// const DB_URI = "mongodb+srv://khoayen:khoamalab_B1@cluster0-yvppm.mongodb.net/test__KHOA"

export class DatabaseService {
    constructor() {
        this.uri = DB_URI;
        this.options = {
            useNewUrlParser: true,
            autoIndex: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        };
        this.connect();
    }

    connect() {
        connect(this.uri, this.options, (err) => {
            console.log(err || `Database connected to ${this.uri}`);
        })
    }
}