import { Router } from "express";
const router = Router();

import { getPlaces, removePlace, addPlace, getPlace, findPlaceById, removePlaces } from "../controllers/place.controller";
import { validateAddPlaceRequest } from './../commons/middlewares/place.validator';

router.route("/place")
    .get(getPlaces)
    .post(validateAddPlaceRequest, addPlace)
    .delete(removePlaces);

router.route("/place/:placeId")
    .get(getPlace)
    .delete(removePlace);

router.param("placeId", findPlaceById)

export { router as placeRouter }