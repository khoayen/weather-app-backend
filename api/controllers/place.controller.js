import { Place } from "../models/place.model";
import { CustomError } from "../commons/error";
import { CustomResponse } from "../commons/response";

export async function findPlaceById(req, res, next) {
    try {
        const placeId = req.params.placeId;
        const foundPlace = await Place.findOne({ _id: placeId });
        if (foundPlace) {
            req.place = foundPlace;
            return next();
        }
        throw new CustomError({ statusCode: 404, error: "Place not found" })
    } catch (error) {
        next(error)
    }
}

export async function getPlaces(req, res, next) {
    try {
        const places = await Place.find();
        new CustomResponse({ result: places, statusCode: 200 }).send(res);

    } catch (error) {
        next(error);
    }
}

export async function getPlace(req, res, next) {
    try {
        new CustomResponse({ result: req.place, statusCode: 200 }).send(res);
    } catch (error) {
        next(error)
    }
}

export async function addPlace(req, res, next) {

    try {
        // check duplicate
        const existedPlace = await Place.find({ lat: req.body.lat, long: req.body.long });
        if (existedPlace.length > 0) {
            throw new CustomError({ statusCode: 412, error: "Place has already been saved" })
        }

        const newPlace = new Place(req.body);
        newPlace.save((error, place) => {
            if (error) {
                throw error;
            }
            else {
                return new CustomResponse({ statusCode: 201, result: place }).send(res);
            }
        })
    } catch (error) {
        next(error)
    }

}

export async function removePlace(req, res, next) {
    try {
        const result = await Place.deleteOne({ _id: req.place._id });
        new CustomResponse({result: "Successfully removed", statusCode: 204}).send(res)
    } catch (error) {
        next(error)
    }
}

export async function removePlaces(req, res, next) {
    try {
        const result = await Place.deleteMany();
        new CustomResponse({result: "Successfully removed", statusCode: 204}).send(res)
    } catch (error) {
        next(error)
    }
}

