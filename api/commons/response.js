class CustomResponse {
    constructor({ result, statusCode = 200 }) {
        this.statusCode = statusCode;
        this.result = result;
    }
    send(res) {
        res.status(this.statusCode).json(this);
    }
}

export { CustomResponse };