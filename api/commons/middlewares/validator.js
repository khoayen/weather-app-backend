import { CustomError } from "../error";

const options = {
    abortEarly: false
  };

export const validationError = (schema, data) => {
    const { error } = schema.validate(data, options);
    if (error) {
      const errorList = error.details.map(error =>
        error.message
          .replace(/\[/g, "")
          .replace(/\]/g, "")
          // eslint-disable-next-line no-useless-escape
          .replace(/\"/g, "'")
          .replace(/child /g, "")
      );
      return new CustomError({
        statusCode: 412,
        error: errorList
      });
    }
    return null;
  };