import Joi from "@hapi/joi";
import { validationError } from './validator';

const placeSchema = Joi.object({
    lat: Joi.number().required(),
    long: Joi.number().required(),
    name: Joi.string(),
    address: Joi.string(),
    phoneNumber: Joi.string(),
    rating: Joi.number()
})

export const validateAddPlaceRequest = async function (req, res, next) {
    try {
        const error = validationError(placeSchema, req.body);
        if (error)
            throw error;
        else return next()
    }
    catch (error) {
        next(error)
    }
}