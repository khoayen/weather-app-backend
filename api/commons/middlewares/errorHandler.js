import { CustomError } from "../error";

export const notFoundError = (req, res, next) => {
    throw new CustomError({ statusCode: 404, error: "Page not found" });
};

export const errorHandler = (err, req, res, next) => {
    // console.log(err);
    if (err instanceof CustomError) {
        return res.status(err.statusCode).json(err);
    }
    res.status(500).json(
        new CustomError({ statusCode: 500, error: "Server error" })
    );
};
