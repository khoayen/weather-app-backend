import { Schema, model } from "mongoose";

const placeSchema = new Schema({
    lat:
    {
        type: Number,
        required: true
    },
    long:
    {
        type: Number,
        required: true
    },
    name:
    {
        type: String
    },
    address:
    {
        type: String,
        default: "Unknown"
    },
    phoneNumber:
    {
        type: String
    },
    rating:
    {
        type: Number
    }
},
    {
        timestamps: true

    });

// Assign address to name in case of null name
placeSchema.pre("save", function () {
    if (!this.name) {
        if(this.address === "Unknown"){
            this.name = `${this.lat}, ${this.long}`;
        } else
        this.name = this.address;
    }
})

export const Place = model("Place", placeSchema);



